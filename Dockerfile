FROM nginx:1.27.3-alpine

# Get supervisor
RUN apk add --update \
  supervisor \
  certbot \
  bash \
  curl \
  openssl \
  && rm -rf /var/cache/apk/*

# Copy in scripts
COPY ./scripts/* /usr/local/bin/
RUN chmod +x \
  /usr/local/bin/cert_renew.sh \
  /usr/local/bin/entrypoint.sh \
  /usr/local/bin/healthcheck.sh

# Copy in configs
COPY confs/nginx.conf /etc/nginx/nginx.conf
COPY confs/healthcheck.conf /etc/nginx/conf.d/healthcheck.conf
COPY confs/pre_cert.conf /etc/nginx/pre_cert.conf
COPY confs/letsencrypt.conf /etc/nginx/letsencrypt.conf
COPY confs/supervisord.conf /etc/supervisor/supervisord.conf
COPY confs/cert_renew.conf /etc/supervisor/conf.d/cert_renew.conf.live
RUN touch /etc/supervisor/conf.d/cert_renew.conf

HEALTHCHECK CMD ["healthcheck.sh"]

RUN mkdir /etc/letsencrypt
RUN mkdir /etc/nginx/sites-enabled
RUN mkdir /etc/nginx/sites-available
RUN mkdir -p /var/lib/letsencrypt
RUN mkdir -p /var/www/letsencrypt/.well-known/acme-challenge

# Remove old Nginx defaults
RUN rm /etc/nginx/conf.d/default.conf

RUN adduser -D -H -u 1000 -s /bin/bash www-data -G www-data

# Permission changes
RUN chown -R www-data \
  /var/log/nginx \
  /var/cache/nginx/ \
  /etc/nginx \
  /etc/letsencrypt \
  /etc/supervisor\
  /var/lib/letsencrypt \
  /usr/share/nginx \
  /var/www

# no var lib nginx???

# TODO: can we not 777
RUN chmod 777 \
  /var/log \
  /var/run \
  /run \
  /tmp

USER www-data

EXPOSE 8080 4433

CMD ["entrypoint.sh"]
