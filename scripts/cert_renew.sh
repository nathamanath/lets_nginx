#!/bin/bash

RENEWAL_PERIOD=${RENEWAL_PERIOD:-"1d"}

echo "Renewing certificates every $RENEWAL_PERIOD"

while :
do
  sleep $RENEWAL_PERIOD
  echo "Renewing certificates"

  # Run the certificate renewal
  certbot renew -c /etc/letsencrypt/cli.ini --deploy-hook "nginx -s reload"
done
