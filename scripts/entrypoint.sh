#!/bin/bash

set -e

echo "Nginx & Let's Encrypt"
nginx -v
certbot --version

# Create log directories
if [ ! -d /var/log/nginx ]
then
  mkdir /var/log/nginx
fi

# Letsencrypt initialisation
if [ -f /etc/letsencrypt/cli.ini ]
then
  echo "Found Letsencrypt cli.ini file"

  # Setup auto renewal
  echo "Adding in auto-renew script for Supervisord"
  ln -sf /etc/supervisor/conf.d/cert_renew.conf.live /etc/supervisor/conf.d/cert_renew.conf
fi

# Link files from sites available to sites enabled
echo "Adding site configurations"
ls -1 /etc/nginx/sites-available | xargs -I{} ln -sf /etc/nginx/sites-available/{} /etc/nginx/sites-enabled/{}

# Take over as init please
echo "Starting Supervisord"
exec /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
