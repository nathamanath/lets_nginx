IMAGE_NAME = nathamanath/lets_nginx
REGISTRY_URL = registry.gitlab.com
VERSION = $(shell cat version.txt)

release: build push

build:
	@echo Build Docker images
	docker build -t $(IMAGE_NAME) .
	docker tag $(IMAGE_NAME) $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)
	docker tag $(IMAGE_NAME) $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)-nginx-1.27.3
	docker tag $(IMAGE_NAME) $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)-nginx-1.27.3
	docker tag $(IMAGE_NAME) $(REGISTRY_URL)/$(IMAGE_NAME):latest

push:
	@echo Pushing to Docker registry
	docker push $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)
	docker push $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)-nginx-1.27.3
	docker push $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)-nginx-1.27.3
	docker push $(REGISTRY_URL)/$(IMAGE_NAME):latest

release: build push
